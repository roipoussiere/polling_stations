# Géolocalisation des bureaux de vote

- démo : [polling_stations](https://roipoussiere.frama.io/polling_stations/)
- données : [data.gouv.fr](https://www.data.gouv.fr/fr/datasets/geolocalisation-des-bureaux-de-vote)
- widget carte : [leaflet](https://leafletjs.com/)
- fournisseur carte : [OSM France](http://www.openstreetmap.fr)
- carte : [OpenStreetMap](https://www.openstreetmap.org/copyright)

## Mise à jour des données

Le script `resources/geo_votes_to_js.py` permet de convertir le dataset téléchargé en fichier js directement utilisable par l'appli web.

```bash
cd resources
wget http://data.cquest.org/elections/bureaux_de_vote/geo_bureaux_de_vote.csv.gz
python geo_votes_to_js.py
```
