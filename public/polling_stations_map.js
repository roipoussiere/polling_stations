var map = L.map('map', {
    preferCanvas: true
}).setView([43.60, 1.43], 13);

var BallotIcon = L.Icon.extend({
  options: {
    // shadowUrl: 'icons/shadow_ballot_icon.png', // not really beautiful
    // iconSize:     [25, 35], // not necessary
    // shadowSize:   [77, 22],
    iconAnchor:   [12, 35],
    shadowAnchor: [12, 20],
    popupAnchor:  [0, -40]
  }
});

var blue_ballot_icon = new BallotIcon({iconUrl: 'icons/blue_ballot_icon.png'});
var orange_ballot_icon = new BallotIcon({iconUrl: 'icons/orange_ballot_icon.png'});
var red_ballot_icon = new BallotIcon({iconUrl: 'icons/red_ballot_icon.png'});
var icons = [red_ballot_icon, orange_ballot_icon, blue_ballot_icon];

L.tileLayer('https://{s}.tile.openstreetmap.fr/osmfr/{z}/{x}/{y}.png', {
  maxZoom: 20,
  attribution: '&copy; <a href="http://www.openstreetmap.fr/">OSM France</a> &amp; <a href="https://www.openstreetmap.org/copyright">OSM</a> contributors'
}).addTo(map);

function show_polling_stations() {
  b = map.getBounds();

  var marker_cluster = L.markerClusterGroup();
  Object.entries(polling_places).filter(([psk, ps]) => ps[0] > b.getSouth() && ps[0] < b.getNorth() && ps[1] > b.getWest() && ps[1] < b.getEast()).forEach(([psk, ps]) => {
      icon = icons[psk % 3];
      tooltip = `<b>${ps[2]}</b><br/><b>n°${ps[3]}</b><br/>${ps[4]}`
      marker_cluster.addLayer(L.marker([ps[0], ps[1]], {icon: icon}).bindPopup(tooltip));
  });
  map.addLayer(marker_cluster);
}

map.on('moveend', function() {
  show_polling_stations();
});
show_polling_stations();
